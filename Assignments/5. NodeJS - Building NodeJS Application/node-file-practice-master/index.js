// //Read File
const fs=require('fs');
const content=fs.readFileSync('./text.txt','utf-8');
console.log(content);
console.log('Things are in sync');

// //Write File
fs.writeFileSync("./addText.txt","\nThis is the new text added in file",{flag:'a'});

// //Rename File
fs.renameSync('./newFolder/demo.txt','./newFolder/newFile.txt');

// //delete File
fs.unlinkSync('./delete.txt');

//Writing the text using Asyn 
fs.writeFile('./addtext.txt',"\nthis is the new text added using asyn write method 2",{flag:'a'},(err,data)=>{
    if(err){
        console.log("Some error encoutered");
    }
    else{
        console.log("Writing text in file is done ");
    }
});
console.log("Asyn writing");

//
const readStream=fs.createReadStream("./text.txt",'utf-8');
const writeStream=fs.createWriteStream("./writeDemo.txt");
readStream.on('data',function(chunk){
    writeStream.write(chunk);
    console.log("Writing the chunk");
})
const writeStream1=fs.createWriteStream("./writeDemoTwo.txt");
readStream.on('data',function(chunk){
    readStream.pipe(writeStream1);
})
fs.renameSync('./writeDemo.txt','./newWriteDemo.txt');

