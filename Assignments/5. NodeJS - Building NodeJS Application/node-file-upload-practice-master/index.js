const http=require("http");
const fs=require("fs");
const formidable=require("formidable");
http.createServer(function(req,res){
    if(req.url=='/filetoupload'){
        let form = new formidable.IncomingForm();
        form.parse(req,function(err,fields,files){
            let oldpath=files.filetoupload.filepath;
            let newpath='./assets/'+files.filetoupload.originalFilename;
            fs.rename(oldpath,newpath,function(err){
                if(err) throw err;
                res.write("File uploaded!");
                res.end();
            });
        });
    }
    else{
    res.writeHead(200,{'Content-Type':"text/html"});
    res.write('<form action="filetoupload" method="post" enctype="multipart/form-data">');
    res.write('<input type ="file" name="filetoupload"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    res.end("up and running ");
    }
}).listen(8000);