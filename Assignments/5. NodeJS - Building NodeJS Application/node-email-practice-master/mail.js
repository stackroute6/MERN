const nodemailer=require("nodemailer");
const transporter=nodemailer.createTransport({
    service:'gmail',
    auth:{
        user:'test@gmail.com',
        pass:'password'
    }
});

const mailoptions={
    from:'test@gmail.com',
    to:'test2@gmail.com',
    subject:'Testing Node Mailer',
    text:'This is a simple email send using mailer'
}

transporter.sendMail(mailoptions,(err,info)=>{
    if(err){
        console.log(err);
    }
    else{
        console.log('Email Sent: '+info.response);
    }
})
