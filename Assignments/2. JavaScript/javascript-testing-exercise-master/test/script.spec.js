const app = require('../script');
const assert = require('chai').assert;
const expect = require('chai').expect;

// Here you need to write the test cases using assert
describe('General Test Cases', () => {
    //This test case should call sayhello() function and check and check the expected string
    it('Should return hello', () => {
        const res = app.sayHello("abc");
        expect(res).to.be.eq("Hello abc");
    });
    //This test case should class factorial function and check the expected result (factorial of a number)
    it('Should return factorial of a number', () => {
        const res = app.Factorial(5);
        expect(res).to.be.eq(120);
    });
    //This test case should call factorial() funtion and check the type of returned value to be a number
    it('Factorial() Should return number', () => {
        const res = app.Factorial(5);
        expect(res).to.be.an("Number");
    });
});

// Here you need to write the test cases using expect
describe('New Test Suite', () => {
    // This test case should call getPerson() function and check the age property in person object
    it('Person should have age property', () => {
        const res = app.getPerson();
        expect(res.age).to.be.an("Number");
    });

    // This test case should call GetUsers() function and check the name property in person object
    it('User should have name property', (done) => {
        const name = app.GetUsers();
        expect(name).to.not.have.own.property('String');
        done();
    });
});
