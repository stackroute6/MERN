fetch('http://localhost:3000/data').then(res=>res.json()).then(data=>console.log(data))
const form=document.querySelector('form');
const adddata=async (e)=>{
    e.preventDefault();
    const sendData={
        id:form.id.value,
        department:form.department.value,
        productName:form.productName.value,
        product:form.product.value
 }
    await fetch('http://localhost:3000/data',{
        method:'POST',
        body:JSON.stringify(sendData),
        headers:{'Content-Type':'application/json'}
    });
}
form.addEventListener('submit',adddata);
