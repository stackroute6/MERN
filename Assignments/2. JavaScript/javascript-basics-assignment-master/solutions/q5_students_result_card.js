// Write a program to display one result card of 100 students
// with their name and percentage
// Out of 100 students, 50 has subjects - Grammer and Accounts
// and other 50 has subjects -  Grammer and Physics

// Hint : You need to calculate percentage of 100 students having different set of subjects.
//        You can assume their scores on their respective subjects.


// Write your code here
const results = () => {
    const studentsList = [
        {name:'Sneha',subjects:[{subject:'Grammer',marks:80},{subject:'Accounts',marks:79}]},
        {name:'Rasika',subjects:[{subject:'Grammer',marks:60},{subject:'Accounts',marks:97}]},
        {name:'Pratiksha',subjects:[{subject:'Grammer',marks:95},{subject:'Accounts',marks:98}]},
        {name:'Giteshwari',subjects:[{subject:'Grammer',marks:88},{subject:'Accounts',marks:49}]},
        {name:'Diksha',subjects:[{subject:'Grammer',marks:70},{subject:'Accounts',marks:78}]},
        {name:'Priti',subjects:[{subject:'Grammer',marks:55},{subject:'Accounts',marks:65}]},
        {name:'Priya',subjects:[{subject:'Grammer',marks:60},{subject:'Accounts',marks:48}]},
    ]
    const result =()=>
    studentsList.reduce((obj,item) =>{

        let percenRes = item.subjects.reduce(()=>{
            return (item.subjects[0].marks+item.subjects[1].marks)/item.subjects.length;
        },{})
        console.log({'name':item.name,'percentage':percenRes});
    },{})
    
    const finResult = result(studentsList);
    return finResult;

}




results();