/* Write a Program to Flatten a given n-dimensional array */

const flatten = () => {
	// Write your code here
	return [].concat.apply([], arr);
};
var arrL1 = [1, [2, 3], [[4], [5]],
console.log(flatten(arrL1)),

/* For example,
INPUT - flatten([1, [2, 3], [[4], [5]])
OUTPUT - [ 1, 2, 3, 4, 5 ]

*/

module.exports = flatten]