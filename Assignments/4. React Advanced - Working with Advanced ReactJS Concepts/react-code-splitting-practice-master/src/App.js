import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";

import Home from "../Components/Home";
import Python from "../Components/Python";
import JavaScript from "../Components/JavaScript";
import BookId from "../Components/BookId";
import "./styles.css";

const LazyBooks = React.lazy(() => import("../Components/Books"));
const LazyManagebook = React.lazy(() => import("../Components/Managebook"));

export default function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="Home" element={<Home />}></Route>
        <Route
          path="Books"
          element={
            <Suspense fallback="Loading">
              <LazyBooks />
            </Suspense>
          }
        ></Route>

        <Route
          path="Managebook"
          element={
            <Suspense fallback="Loading">
              <LazyManagebook />
            </Suspense>
          }>
          <Route path="1" element={<Python />}></Route>
          <Route path="2" element={<JavaScript />}></Route>
        </Route>
        <Route path="BookId" element={<BookId />}>
          <Route
            path="Managebook"
            element={
              <Suspense fallback="Loading">
                <LazyManagebook />
              </Suspense>
            }>
              <Route></Route>
            </Route>
        </Route>
        <Route path="bookid" element={<BookId/>}>
          <Route path="managebooks" element={<Suspense fallback="Loading"><LazyManagebook/></Suspense>}>
            <Route path="1" element={<Python/>}> </Route>
            <Route path="2" element={<JavaScript/>}></Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
}
