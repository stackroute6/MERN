import { Link, Outlet } from "react-router-dom";

const Managebook = () => {
  return (
    <>
      <ul>
        <li>
          Python &nbsp;&nbsp;
          <Link to="1">
            <button>Manage</button>
          </Link>
        </li>
        <li>
          JavaScript &nbsp;&nbsp;
          <Link to="2">
            <button>Manage</button>
          </Link>
        </li>
      </ul>
      <Outlet></Outlet>
    </>
  );
};
export default Managebook;
