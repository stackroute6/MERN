import React from "react";
const initialList = [
  {
    id: "a",
    name: "Python"
  },
  {
    id: "b",
    name: "JavaScript"
  }
];

const Books = () => {
  const [list, setList] = React.useState(initialList);
  const [name, setName] = React.useState("");

  function handleChange(event) {
    setName(event.target.value);
  }

  function handleAdd(e) {
    e.preventDefault();
    const newList = list.concat({ name });
    setName("");

    setList(newList);
  }

  return (
    <form>
      <center>
        <div>
          <h1>Adding a new book to the Library :</h1>
          <div>
            <label htmlFor="">Enter Your Book - &nbsp;&nbsp;</label>
            <input
            size="30"
            type="text"
            value={name}
            onChange={handleChange}
            placeholder="Enter Your Books Here"
            />
            <button type="submit" onClick={handleAdd}>
              Add Books
            </button>
          </div>

          <ul>
            {list.map((item) => (
              <li key={item.id}>{item.name}</li>
            ))}
          </ul>
        </div>
      </center>
    </form>
  );
};

export default Books;
