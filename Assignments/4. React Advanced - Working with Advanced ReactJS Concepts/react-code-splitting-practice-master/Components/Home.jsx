const initialList = [
  {
    id: "a",
    name: "Python"
  },
  {
    id: "b",
    name: "JavaScript"
  },
];
const Home = () => {
  return (
    <>
      <h1>Available Books in the Library :</h1>
      <ul>
        {initialList.map((item) => (
          <li key={item.id}>{item.name}</li>
        ))}
      </ul>
    </>
  );
};
export default Home;
