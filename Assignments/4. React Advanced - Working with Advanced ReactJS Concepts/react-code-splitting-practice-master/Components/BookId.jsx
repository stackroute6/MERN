import { Link, Outlet } from "react-router-dom";

const BookId = () => {
  return (
    <>
      <ul>
        <li>
          Python &nbsp;&nbsp;
          <Link to="Managebook">
            <button>delete</button>
          </Link>
        </li>
        <li>
          JavaScript &nbsp;&nbsp;
          <Link to="Managebook">
            <button>Delete</button>
          </Link>
        </li>
      </ul>
      <Outlet></Outlet>
    </>
  );
};
export default BookId;
