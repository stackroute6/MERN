import Greet from './greet';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Hello</h1>
      <Greet/>
    </div>
  );
}

export default App;
