import logo from './logo.svg';
import './App.css';
import summary from './summary';
import experience from './experience';
import contact from './contact';
import header from './header';
function App() {
  return (
    [header(),summary(),experience(),contact()]
  );
}

export default App;
