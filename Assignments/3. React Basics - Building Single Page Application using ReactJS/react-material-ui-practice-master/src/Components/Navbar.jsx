import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";

export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }} className="box2">
      <AppBar position="static" color="inherit">
        <Toolbar>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ mr: 2, display: { xs: "none", md: "flex" }, color: "blue" }}
          >
            SUBSCRIBE
          </Typography>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, color: "black" }}
          >
            Blogs
          </Typography>
          <IconButton size="large" aria-label="search" color="inherit">
            <SearchIcon />
          </IconButton>
          <Button color="primary" variant="outlined">
            SIGN UP
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
