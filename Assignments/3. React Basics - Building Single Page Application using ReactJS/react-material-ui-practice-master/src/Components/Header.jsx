import * as React from "react";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

export default function NavTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // const NavTabs = () => {
  return (
    <Box sx={{ width: "100%" }}>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="nav tabs example"
        sx={({ pl: "7" }, { pr: "2" })}
      >
        <LinkTab label="Technology" href="/drafts" />
        <LinkTab label="Design" href="/trash" />
        <LinkTab label="Culture" href="/spam" />
        <LinkTab label="Busnicss" href="/spam" />
        <LinkTab label="Politics" href="/spam" />
        <LinkTab label="Opinion" href="/spam" />
        <LinkTab label="Science" href="/spam" />
        <LinkTab label="Health" href="/spam" />
        <LinkTab label="Style" href="/spam" />
        <LinkTab label="Travel" href="/spam" />
      </Tabs>
    </Box>
  );
}

// export default NavTabs;
