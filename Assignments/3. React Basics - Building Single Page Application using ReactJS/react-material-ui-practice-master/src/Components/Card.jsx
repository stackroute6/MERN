import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import one from "./one.jpg";

import { CardActionArea, CardActions } from "@mui/material";

export default function MultiActionAreaCard() {
  return (
    <Box sx={({ flexGrow: 1 }, { mt: "5" })}>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <Card sx={{ maxWidth: 345 }}>
            <CardActionArea>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h6"
                  component="div"
                  sx={{ textAlign: "left" }}
                >
                  Featured Post
                </Typography>
                <Typography
                  variant="body2"
                  color="text.secondary"
                  sx={({ mb: 1.5 }, { textAlign: "left" })}
                >
                  Nov 12,
                  <br /> This is wider card with supporting text below as
                  natuaral read-in to an addition content
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Link href="#">Continue to reading...</Link>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={1} sx={{ ml: -2 }}>
          <CardMedia
            component="img"
            height="185"
            image={one}
            alt="green iguana"
            sx={({ ml: -2 }, { pl: 0 })}
          />
        </Grid>
        <Grid item xs={2}></Grid>
        <Grid item xs={3}>
          <Card sx={{ maxWidth: 345 }}>
            <CardActionArea>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h6"
                  component="div"
                  sx={{ textAlign: "left" }}
                >
                  Featured Post
                </Typography>
                <Typography
                  variant="body2"
                  color="text.secondary"
                  sx={({ mb: 1.5 }, { textAlign: "left" })}
                >
                  Nov 11,
                  <br /> This is wider card with supporting text below as
                  natuaral read-in to an addition content
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Link href="#">Continue to reading...</Link>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={1} sx={{ ml: -2 }}>
          <CardMedia
            component="img"
            height="185"
            image={one}
            alt="green iguana"
            sx={({ ml: -2 }, { pl: 0 })}
          />
        </Grid>
      </Grid>
    </Box>
  );
}
