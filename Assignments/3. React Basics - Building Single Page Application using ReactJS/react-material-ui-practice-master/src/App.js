import Blog from "./Blog";
import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <Blog />
    </div>
  );
}
