import { useState } from "react";
import Todo from "./Components/Todo";
import "./styles.css";

export default function App() {
  const [list,setList]= useState([
    {
      todoItem: "Don't Go",
      id: 1
    },
    {
      todoItem: "Go Go",
      id: 2
    },
  ]);
  
  const [todo,setTodo]= useState("");
  const [id,setId]= useState();

  const handleTodo =(event) => {
    setTodo(event.target.value);
  };
  const handleId=(event)=>{
    setId(event.target.value);
  }
  const handleSubmit=(event)=>{
    event.preventDefault();
    setList([...list, { id:id,todoItem: todo }]);
    setTodo("");
  };
  const handleDelete=(id)=>{
    const newList=list.filter(item=>item.id!==id);
    setList(newList);
  };
  return (
    <>
    <div>
      <form action="">
      <label>Enter to Do</label>
      <input type="text" placeholder="Enter to Do" onChange={handleTodo} value={todo} />
      <label>Enter to Do</label>
      <input type="text" placeholder="Enter to Do" onChange={handleId} value={id} />
      <button type="submit" onClick={handleSubmit}>Add Todo</button>
      <button type="submit" onClick={handleSubmit}>Add Todo</button>
      </form>
      </div>
     <Todo list={list} handleDelete={handleDelete} />
    </>   
  );
}
