import { Link } from "react-router-dom";

const Home = () => {
  return (
    <>
      <nav>
        <Link to="contact">Add Books</Link>
        <br />
        <Link to="manage-books">Manage Books</Link>
      </nav>
      <h1>Available Books</h1>
      <ul>
        <li>Wolf Hall</li>
        <li>Gilead</li>
        <li>Secondhand Time</li>
      </ul>
    </>
  );
};
export default Home;
