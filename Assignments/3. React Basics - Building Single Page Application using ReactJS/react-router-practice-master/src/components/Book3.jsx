import { Link } from "react-router-dom";
const Book3 = () => {
  return (
    <>
      <h1>Secondhand Time</h1>
      <h2>Book Details</h2>
      <ul>
        <li>Book Name : Secondhand Time </li>
        <li>Author : Svetlana Alexievich</li>
        <li>Publish : 2013</li>
      </ul>

      <Link to="/manage-books">Delete</Link>
    </>
  );
};
export default Book3;
