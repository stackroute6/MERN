import { Link, Outlet } from "react-router-dom";

const Books = () => {
  return (
    <>
      <ul>
        <li>
          Wolf Hall <Link to="1">Manage</Link>
        </li>
        <li>
          Gilead <Link to="2">Manage</Link>
        </li>
        <li>
          Secondhand Time <Link to="3">Manage</Link>
        </li>
      </ul>
      <Outlet></Outlet>
    </>
  );
};
export default Books;
