import { Link } from "react-router-dom";
const Book2 = () => {
  return (
    <>
      <h1>Gilead</h1>
      <h2>Book Details</h2>
      <ul>
        <li>Book Name : Gilead </li>
        <li>Author : Marilynne Robinson</li>
        <li>Publish : 2004</li>
      </ul>

      <Link to="/manage-books">Delete</Link>
    </>
  );
};
export default Book2;
