import { Link } from "react-router-dom";
const Book1 = () => {
  return (
    <>
      <h1>Wolf Hall</h1>
      <h2>Book Details</h2>
      <ul>
        <li>Book Name : Wolf Hall </li>
        <li>Author : Hilary Mantel</li>
        <li>Publish : 2009</li>
      </ul>

      <Link to="/manage-books">Delete</Link>
    </>
  );
};
export default Book1;
