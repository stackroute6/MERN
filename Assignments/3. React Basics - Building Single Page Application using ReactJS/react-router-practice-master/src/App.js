import { Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import "./styles.css";
import Contact from "./components/Contact";
import Books from "./components/Books";
import Book1 from "./components/Book1";
import Book2 from "./components/Book2";
import Book3 from "./components/Book3";

export default function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="about" element={<About />}></Route>
        <Route path="contact" element={<Contact />}></Route>
        <Route path="manage-books" element={<Books />}>
          <Route path=":bookName" element={<Book1 />}></Route>
          <Route path="2" element={<Book2 />}></Route>
          <Route path="3" element={<Book3 />}></Route>
        </Route>
      </Routes>
    </div>
  );
}
