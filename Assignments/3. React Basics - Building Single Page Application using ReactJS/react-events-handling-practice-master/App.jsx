import './App.css';
import { useState } from 'react'

function App() {
  const [count, setCount] = useState(0);
  const [lbtnCount, setLbtnCount] = useState();
  const [btnCount, setbtnCount] = useState([]);

  const displayLBtn = (cc) => {
    setLbtnCount(parseInt(cc) + 1);
  };

  const onAddBtnClick = () => {
    setCount(count + 1);
    setbtnCount(btnCount.concat(<button value = {count} onClick={e => displayLBtn(e.target.value)}>BUTTON - {count + 1}</button>));
  };

  return (
    <>
      <button>COUNTER - {count}</button>
      <button>LAST BUTTON CLICKED - {lbtnCount}</button><br />
      <button onClick={onAddBtnClick}>ADD A BUTTON</button>
      {btnCount}
    </>
  );
}

export default App;
